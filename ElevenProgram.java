package week1;

public class ElevenProgram {
    public static void main(String args[]) {
        int[] arr = {1, 5, 33, 12, 88, 9, 192, 123, 567, 88, 44, 32};

        for (int curr_arr : arr) {
            int result = curr_arr % 2 == 0 ? curr_arr : curr_arr * 10;
                System.out.println(result);
        }
    }

}

